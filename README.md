# REPO is outdated! For pyconfigdb package refer to the /pyconfigdb directory in the [itk-demo-config repo](https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/itk-demo-configdb/-/tree/master/pyconfigdb).


# pyconfigdb

**pyconfigdb** is a wrapper for the ConfigDB endpoints. It contains several functions to access the HTML endpoints in python code. For more information on the functionalities, please refer to the ConfigDB API.

## Installation
To install this package and use it simply install it via pip (preferably from inside a venv). This will also enable you to run the CLI by running pyconfigdb from you terminal.
```bash
pip install pyconfigdb --extra-index-url https://gitlab.cern.ch/api/v4/groups/33370/-/packages/pypi/simple
```
To upgrade run:
```bash
pip install --upgrade pyconfigdb --extra-index-url https://gitlab.cern.ch/api/v4/groups/33370/-/packages/pypi/simple
```

The cli is also available as a container. For an example please refer to the [extended tutorial](https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/deployments/tutorial/-/tree/extended_tutorial?ref_type=heads).

## CLI
*cli.py* implements a command line interface. For an exemplary bash script using cli.py, please see *example.sh*.

The example script sets up a dummy runkey *(dummy_rk)* and writes it to the disk. This dummy_rk consists of two felixCards with two felixDevices each, as well as two payloads attached to each felixCard and felixDevice.

The script then uses cli.py to import the dummy_rk from the disk to the staging area. It then commits it to the database, clones it back into the staging area and exports it back to the disk with the name *exported_dummy*. \
It is also possible to directly commit the runkey to the database on import. For an example on how to do this, please refer to the commented lines in the script.

Then, all runkeys are listed with name, author and comment. Without the flag -b, *runkeys -t "runkey"* lists all runkeys from the stage. With -b, all the runkeys from the backend database are listed.

Lastly for cleanup, the script deletes the cloned dummy runkey from the staging area.