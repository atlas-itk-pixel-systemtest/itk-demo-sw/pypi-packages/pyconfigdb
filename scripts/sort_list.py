import json

lls_pos_mod_ID_array: list[tuple[int, str]]


def main():
    with open("scripts/LLS_EC_0001.json", "r") as file:
        data = json.load(file)

    modules = [
        (d["metadata"]["position"], d["metadata"]["serial"], d["metadata"]["paths"])
        for d in data["objects"]
    ]
    sorted_modules = sorted(modules, key=lambda x: x[0])
    print(modules)
    print(sorted_modules)

    paths_list = [d["metadata"]["paths"] for d in data["objects"] if (d["metadata"]["position"] == 1)]
    print(paths_list)


if __name__ == "__main__":
    main()
