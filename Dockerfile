FROM gitlab-registry.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/demi-baseimage/base:1

COPY . .
RUN python3 -m pip install --upgrade pip
RUN python3 -m pip install . --extra-index-url https://gitlab.cern.ch/api/v4/groups/33370/-/packages/pypi/simple

RUN chmod 777 /root/.bashrc

CMD ["s6-setuidgid", "itk", "bash", "--login" ]