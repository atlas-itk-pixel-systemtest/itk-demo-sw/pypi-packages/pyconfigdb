from pyconfigdb.exceptions import ConfigDBConnectionError, ConfigDBResponseError
import json
import os
from requests import post, get, delete
from requests.exceptions import ConnectionError
from pyconfigdb.tools import export_from_dict, create_import_dict, read_scan_configs
from pyregistry import ServiceRegistry


class ConfigDB:
    def __init__(
        self,
        dbKey,
        srUrl="http://localhost:5111/api",
        headers={"content-type": "application/json", "accept": "application/json"},
    ):
        self.dbKey = dbKey
        self.headers = headers

        sr = ServiceRegistry(srUrl)

        self.dbUrl = sr.get(self.dbKey)

        if not self.health():
            raise ConfigDBConnectionError(self.dbUrl)

    def _standard_post(self, endpoint, body, parameter, default_msg):
        try:
            response = post(
                f"{self.dbUrl}{endpoint}",
                params=parameter,
                data=json.dumps(body),
                headers=self.headers,
            )
        except ConnectionError:
            raise ConfigDBConnectionError(self.dbUrl)

        if not response.status_code // 100 == 2:
            raise ConfigDBResponseError(response, default_msg)

        return response.json()

    def _standard_get(self, endpoint, parameter, default_msg):
        try:
            response = get(f"{self.dbUrl}{endpoint}", params=parameter, headers=self.headers)
        except ConnectionError:
            raise ConfigDBConnectionError(self.dbUrl)

        if not response.status_code // 100 == 2:
            raise ConfigDBResponseError(response, default_msg)

        return response.json()

    def _standard_delete(self, endpoint, parameter, default_msg):
        try:
            response = delete(f"{self.dbUrl}{endpoint}", params=parameter, headers=self.headers)
        except ConnectionError:
            raise ConfigDBConnectionError(self.dbUrl)

        if not response.status_code // 100 == 2:
            raise ConfigDBResponseError(response, default_msg)

        return response.json()

    # High-level endpoints

    def health(self):
        """
        Check health of configDB

        Returns
        -------
        True if successfull
        """
        try:
            self._standard_get("/health", {}, "")
        except (ConfigDBConnectionError, ConfigDBResponseError):
            return False

        return True

    def add_node(self, type: str, id: str = None, payloads: list = [], parents: list = [], children: list = [], stage: bool = True):
        """
        Adds an object with payloads.

        Parameters
        ----------
        type : str
            Type of the created object.
        id : str
            uuid of the object to create.
        payloads : list
            List of payload dicts or IDs.
        parents : list
            List of connections (dict of "id" and "view" option) for parents.
        children : list
            List of connections (dict of "id" and "view" option) for children.
        stage : bool
            Defines whether the request should be executed on the backend database (False) or staging area (True). Default is True.

        Returns
        -------
        res["id"] : ID of the created object.
        """
        res = self._standard_post(
            "/node",
            {"payloads": payloads, "parents": parents, "children": children, "type": type, "id": id},
            {"stage": stage},
            "Could not add node.",
        )

        return res["id"]

    def add_nodes(self, mylist: list = [], stage: bool = True):
        """
        Adds multiple objects, optionally including new payloads.

        Parameters
        ----------
        mylist : list
            List of objects to be inserted. See add_node for the structure of the objects.
        stage : bool
            Defines whether the request should be executed on the backend database (False) or staging area (True). Default is True.

        Returns
        -------
        res["ids"] : List of IDs of the created objects.
        """
        res = self._standard_post(
            "/nodes",
            {"list": mylist},
            {"stage": stage},
            "Could not add nodes.",
        )

        return res["ids"]

    def add_to_node(self, id: str = None, children: list = [], payloads: list = [], stage: bool = True):
        """
        Adds payloads to an existing node.

        Parameters
        ----------
        id : str
            UUID of the node.
        parents : list
            List of connections (dict of "id" and "view" option) for parents.
        children : list
            List of connections (dict of "id" and "view" option) for children.
        stage : bool
            Defines whether the request should be executed on the backend database (False) or staging area (True). Default is True.

        Returns
        -------
        res["ids"] : IDs of the newly added payloads.
        """
        res = self._standard_post(
            "/on_node",
            {"children": children, "payloads": payloads},
            {"id": id, "stage": stage},
            "Could not add payloads.",
        )

        return res["ids"]

    def payloads_bundle(self, ids: list[str]):
        """
        Bundles content of multiple json payloads into one.

        Parameters
        ----------
        ids : list[str]
            list of uuids to bundle

        Returns
        -------
        bundled payloads dict
        """
        res = self._standard_get(
            "/payloads",
            {"ids": ids},
            "Could not bundle payloads.",
        )

        return res["bundle"]

    def payloads_change(self, changes: str = None, associations: list = [dict]):
        """
        Changes values of mulitple payloads at once

        Parameters
        ----------
        changes : str
            dict representing the changes to be made to all payloads, in order to change list elements, simply use the index as key and the new value as the value
        associations : list
            list of associations (dict with payload and object ids)

        Returns
        -------
        list of newly created payloads
        """
        res = self._standard_post(
            "/payloads",
            {"changes": changes, "associations": associations},
            {},
            "Could not change payloads.",
        )

        return res["ids"]

    def remove_from_node(self, id: str = None, children: list = [], payloads: list = []):
        """
        Removes payloads and/or children from an existing node.

        Parameters
        ----------
        id : str
            uuid of the node.
        children : list
            List of children to be deleted.
        payloads : list
            List of payloads to be deleted.
        """
        res = self._standard_delete(
            "/on_node",
            {"id": id, "children": children, "payloads": payloads},
            "Could not remove from node.",
        )

        return None

    def pdb_import(
        self,
        itkdb_access_code1: str,
        itkdb_access_code2: str,
        name: str,
        serials: list,
        author: str = "",
        comment: str = "",
        type: str = "",
        count: bool = False,
    ):
        """
        Imports module configurations from the production database.

        Parameters
        ----------
        itkdb_access_code1/_code2 : str
            Access codes 1 and 2 for the itkdb, respectively.
        name : str
            Name of the tag with which the modules are saved.
        serials : list
            List of module serial numbers.
        author, comment, type : str
            Author, comment and type of the tag, respectively.
        count: if True, modules serials are extended with a counter

        Returns
        -------
        None.
        """
        res = self._standard_post(
            "/pdb_import",
            {
                "author": author,
                "comment": comment,
                "itkdb_access_code1": itkdb_access_code1,
                "itkdb_access_code2": itkdb_access_code2,
                "name": name,
                "serials": serials,
                "type": type,
            },
            {"count": count},
            "Could not import module configurations.",
        )

        return None

    def search(self, identifier: str, object_type: str = "", config_type: str = "", search_dict: dict = {}):
        """
        Gets the payloads of an object of a specific type with specific metadata.

        Parameters
        ----------
        identifier : str
            Name of the root tag or UUID of the root node.
        object_type : str
            Type of the searched object.
        config_type : str
            Type of the searched configuration.
        search_dict : dict
            Dictionary with keys and values to filter by.
            key : str
            value : str

        Returns
        -------
        res["data"] : list of objects and their payloads
        """
        res = self._standard_post(
            "/search",
            {
                "search_dict": search_dict,
            },
            {"identifier": identifier, "object_type": object_type, "config_type": config_type},
            "Could not get payloads.",
        )

        return res["data"]

    def search_in_tag(
        self,
        name: str,
        payload_types: list[str] = [],
        object_ids: list[str] = [],
        search_dict: dict = {},
        payload_data: bool = False,
        order_by_object: bool = False,
    ):
        """
        Gets the payloads of an object of a specific type with specific metadata.

        Parameters
        ----------
        name : str
            Name of the tag
        payload_types : list[str]
            List of types to filter by
        object_ids : list[str]
            List of object ids to filter by
        search_dict : dict
            Dictionary with keys and values to filter by.
            key : str
            value : str
        payload_data: bool
            adds data of payload
        order_by_object: bool
            order payload types by object id

        Returns
        -------
        res["data"] : list of payloads
        """
        res = self._standard_post(
            "/search/tag",
            {"payload_types": payload_types, "search_dict": search_dict, "object_ids": object_ids},
            {"name": name, "payload_data": payload_data, "order_by_object": order_by_object},
            "Could not search tag.",
        )

        return res["data"]

    def search_subtree(
        self,
        identifier: str,
        object_type: str = "",
        search_dict: dict = {},
        payload_data: bool = False,
        depth: int = -1,
        view: int = 1,
    ):
        """
        Gets the subtrees with a root node of a specific type with specific metadata.

        Parameters
        ----------
        identifier : str
            Name of the root tag or UUID of the root node.
        object_type : str
            Type of the searched object.
        search_dict : dict
            Dictionary with keys and values to filter by.
            key : str
            value : str
        payload_data: bool
            adds data of payload to each dataset
        depth: int
            defines depth level to which the tree should be read (-1 for full tree)
        view : int
            Sets the view of the tree. Default is 1. Set 2 for stale and 3 for both.

        Returns
        -------
        res : list of subtrees
        """
        res = self._standard_post(
            "/search/subtree",
            {"search_dict": search_dict},
            {"identifier": identifier, "object_type": object_type, "payload_data": payload_data, "depth": depth, "view": view},
            "Could not get subtree.",
        )

        return res["data"]  # TODO

    # Stage endpoints

    def stage_delete(self, identifier: str):
        """
        Deletes a runkey from the staging area.

        Parameters
        ----------
        identifier : str
            Name of the root tag or uuid of the root node.

        Returns
        -------
        res["id"] : id of the deleted runkey.
        """
        res = self._standard_delete(
            "/stage",
            {"identifier": identifier},
            "Could not delete runkey.",
        )

        return res["id"]

    def stage_new(self, type: str = "", author: str = "", comment: str = "", name: str = None, payloads: list = []):
        """
        Creates a new root node in the staging area.

        Parameters
        ----------
        type, author, comment : str
            Type, author and comment of the tag, respectively.
        name : str
            If given, a tag with this name is created for the root node.
        payloads : list
            List of payloads of the newly created node.

        Returns
        -------
        res["id"] : id of the newly created node.
        """
        res = self._standard_post(
            "/stage/root",
            {"type": type, "author": author, "comment": comment, "name": name, "payloads": payloads},
            {},
            "Could not create node.",
        )

        return res["id"]

    def stage_create(self, data: dict, type: str = "", author: str = "", comment: str = "", name: str = None):
        """
        Creates an new tree or subtree.

        Parameters
        ----------
        data : dict
            (Sub-)tree to be added.
        type, author, comment : str
            Type, author and comment of the tag, respectively.
        name : str
            If given, a tag with this name is created for the root node.

        Returns
        -------
        res["id"] : id of the root node of the newly created tree.
        """
        res = self._standard_post(
            "/stage",
            {"type": type, "author": author, "comment": comment, "data": data, "name": name},
            {},
            "Could not create tree.",
        )

        return res["id"]

    def stage_clone(self, identifier: str, new_name: str, type: str = "", author: str = "", comment: str = "", view: int = 1, keep_ids: bool = False):
        """
        Clones a tagged tree from the database to the staging area.

        Parameters
        ----------
        identifier : str
            Name of the root tag or uuid of the root node in the database.
        new_name : str
            Name to be set for the cloned tree in the staging area.
        type, author, comment : str
            Type, author and comment of the tag, respectively.
        view : int
            Sets the view of the tree. Default is 1. Set 2 for stale and 3 for both.
        keep_ids : bool
            If True, the ids of the objects are kept. Default is False.

        Returns
        -------
        res["ids"] : list id of object uuids of the newly cloned tree.
        """
        res = self._standard_post(
            "/stage/clone",
            {"identifier": identifier, "name": new_name, "type": type, "author": author, "comment": comment, "view": view},
            {"keep_ids": keep_ids},
            "Could not clone tree.",
        )

        return res["ids"]

    def stage_commit(self, identifier: str, new_name: str = "", type: str = "", author: str = "", comment: str = "", view: int = 1, keep_ids: bool = False):
        """
        Commits a tagged tree from the staging area to the database.

        Parameters
        ----------
        identifier : str
            Name of the root tag or uuid of the root node in the staging area.
        new_name : str
            Name to be set for the committed tree in the database.
        type, author, comment : str
            Type, author and comment of the tag, respectively.
        view : int
            Sets the view of the tree. Default is 1. Set 2 for stale and 3 for both.
        keep_ids : bool
            If True, the ids of the objects are kept. Default is False.


        Returns
        -------
        res["ids"] : list id of object uuids of the newly committed tree.
        """
        res = self._standard_post(
            "/stage/commit",
            {"identifier": identifier, "name": new_name, "type": type, "author": author, "comment": comment, "view": view},
            {"keep_ids": keep_ids},
            "Could not commit tree.",
        )

        return res["ids"]

    # Low-level endpoints

    def insert(self, insert_list: list, table: str, stage: bool = True):
        """
        Inserts multiple datasets into specified table.

        Parameters
        ----------
        insert_list : list
            List of objects to insert.
        table : str
            Table to insert the list into.
        stage : bool
            Defines whether the request should be executed on the backend database (False) or staging area (True). Default is True.

        Returns
        -------
        None.
        """
        res = self._standard_post(
            "/insert",
            {"list": insert_list},
            {"stage": stage, "table": table},
            "Could not insert objects.",
        )

        return None

    def object_create(self, type, payloads: list = [], parents: list = [], children: list = [], id: str = None, stage: bool = True):
        """
        Sets an object dataset.

        Parameters
        ----------
        type : str
            Type of the object.
        payloads : list
            List of uuids of connected payloads.
        parents : list
            List of connections (dict of "id" and "view" option) for parents.
        children : list
            List of connections (dict of "id" and "view" option) for children.
        id : str
            uuid of the object to create.
        stage : bool
            Defines whether the request should be executed on the backend database (False) or staging area (True). Default is True.

        Returns
        -------
        res["id"] : id of created object
        """
        res = self._standard_post(
            "/object",
            {"payloads": payloads, "parents": parents, "type": type, "id": id, "children": children},
            {"stage": stage},
            "Could not create object.",
        )

        return res["id"]

    def object_read(self, id: str, stage: bool = True, view: int = 1):
        """
        Reads an object dataset.

        Parameters
        ----------
        id : str
            uuid of the object to be read.
        stage : bool
            Defines whether the request should be executed on the backend database (False) or staging area (True). Default is True.
        view : int
            Sets the view of the tree. Default is 1. Set 2 for stale and 3 for both.


        Returns
        -------
        res : Requested scan (without the HTML status code).
        """
        res = self._standard_get(
            "/object",
            {"id": id, "view": view, "stage": stage},
            "Could not read object.",
        )
        res.pop("status")
        return res

    def object_tree(
        self, id: str, stage: bool = True, depth: int = -1, payload_data: bool = False, payload_filter: str = "", view: int = 1, format: bool = False
    ):
        """
        Gets a tree from an object dataset.

        Parameters
        ----------
        id : str
            uuid of the object to be read.
        stage : bool
            Defines whether the request should be executed on the backend database (False) or staging area (True). Default is True.
        depth : int
            Depth up to which the tree will be traversed. Default is -1 (for full tree).
        payload_data : bool
            Defines whether or not to add the payload data. Default is False.
        payload_filter : str
            only include payloads which type contains this string
        view : int
            Sets the view of the tree. Default is 1. Set 2 for stale and 3 for both.
        format : bool
            Defines whether the payload is indented or not. Default is False.

        Returns
        -------
        res : Requested scan (without the HTML status code).
        """
        res = self._standard_get(
            "/object/tree",
            {"stage": stage, "id": id, "depth": depth, "payload_data": payload_data, "payload_filter": payload_filter, "view": view, "format": format},
            "Could not get object tree.",
        )
        res.pop("status")
        return res

    def payload_create(self, type: str, data: str = "", name: str = "", id: str = None, stage: bool = True, meta: bool = False):
        """
        Creates a payload dataset.

        Parameters
        ----------
        data : str
            Data of the payload.
        name : str
            Name of the payload file.
        type : str
            Type of the payload.
        id : str
            uuid of the payload in hex form.
        stage : bool
            Defines whether the request should be executed on the backend database (False) or staging area (True). Default is True.
        meta : bool
            Defines whether the payload is metadata or not. Default is False.

        Returns
        -------
        res["id"] : uuid of the created payload dataset.
        """
        res = self._standard_post(
            "/payload",
            {"data": data, "meta": meta, "name": name, "type": type, "id": id},
            {"stage": stage},
            "Could not create payload.",
        )

        return res["id"]

    def payload_update(self, id: str, type: str = "", data: str = "", name: str = "", meta: bool = False, stage: bool = True):
        """
        Update a payload dataset.

        Parameters
        ----------
        id : str
            uuid of the payload in hex form.
        type : str
            Type of the payload.
        data : str
            Data of the payload.
        name : str
            Name of the payload file.
        meta : bool
            Defines whether the payload is metadata or not. Default is False.
        stage : bool
            Defines whether the request should be executed on the backend database (False) or staging area (True). Default is True.


        Returns
        -------
        True
        """
        res = self._standard_post(
            "/payload/update",
            {"data": data, "meta": meta, "name": name, "type": type, "id": id},
            {"stage": stage},
            "Could not update payload.",
        )

        return True

    def payload_read(self, id: str, stage: bool = True, meta: bool = False, format: bool = False):
        """
        Reads a payload dataset.

        Parameters
        ----------
        id : str
            uuid of the object to be read.
        stage : bool
            Defines whether the request should be executed on the backend database (False) or staging area (True). Default is True.
        meta : bool
            Defines whether the payload is metadata or not. Default is False.
        format : bool
            Defines whether the payload is indented or not. Default is False.

        Returns
        -------
        res : Requested scan (without the HTML status code).
        """
        res = self._standard_get(
            "/payload",
            {"id": id, "meta": meta, "format": format, "stage": stage},
            "Could not get payload.",
        )
        res.pop("status")
        return res

    def tag_create(
        self,
        name: str,
        type: str,
        objects: list = [],
        payloads: list = [],
        groups: list = [],
        members: list = [],
        author: str = "",
        comment: str = "",
        id: str = None,
        stage: bool = True,
    ):
        """
        Creates a tag dataset.

        Parameters
        ----------
        objects : list
            List of uuids of tagged objects.
        payloads : list
            List of uuids of connected payloads.
        groups : list
            List of tag names that this tag should be included in.
        members : list
            List of tag names this tag should include.
        name, author, comment, type : str
            Name, author, comment and type of the tag, respectively.
        id : str
            uuid of the tag to be created.
        stage : bool
            Defines whether the request should be executed on the backend database (False) or staging area (True). Default is True.

        Returns
        -------
        res["id"] : uuid of the created tag dataset.
        """
        res = self._standard_post(
            "/tag",
            {
                "objects": objects,
                "payloads": payloads,
                "groups": groups,
                "members": members,
                "name": name,
                "author": author,
                "comment": comment,
                "type": type,
                "id": id,
            },
            {"stage": stage},
            "Could not create tag.",
        )

        return res["id"]

    def tag_format(self, name: str, stage: bool = True, include_id: bool = False, shorten_data: int = 10):
        """
        Returns a tag dataset formatted as a string.

        Parameters
        ----------
        name : str
            Name of the tag dataset.
        stage : bool
            Defines whether the request should be executed on the backend database (False) or staging area (True). Default is True.
        include_id : bool
            Defines whether to include the id of the datasets. Default is False.
        shorten_data : int
            Amount of characters to shorten the data to. Default is 10. -1 returns the full data.

        Returns
        -------
        res["string"] : Requested string.
        """
        res = self._standard_get(
            "/tag/format",
            {"stage": stage, "include_id": include_id, "name": name, "shorten_data": shorten_data},
            "Could not format tag.",
        )

        return res["string"]

    def tag_read(self, name: str, stage: bool = True):
        """
        Reads a tag dataset.

        Parameters
        ----------
        name : str
            Name of the tag dataset.
        stage : bool
            Defines whether the request should be executed on the backend database (False) or staging area (True). Default is True.

        Returns
        -------
        res : Dataset of the requested tag (without the HTML status code).
        """
        res = self._standard_get(
            "/tag",
            {"stage": stage, "name": name},
            "Could not read tag.",
        )
        res.pop("status")
        return res

    def tag_tree(
        self,
        name: str,
        stage: bool = True,
        depth: int = -1,
        payload_data: bool = False,
        payload_filter: str = "",
        view: int = 1,
        format: bool = False,
    ):
        """
        Gets a tree from an object dataset.

        Parameters
        ----------
        name : str
            Name of the tag to be read.
        stage : bool
            Defines whether the request should be executed on the backend database (False) or staging area (True). Default is True.
        depth : int
            Depth up to which the tree will be traversed. Default is -1 (for full tree).
        payload_data : bool
            Defines whether or not to add the payload data. Default is False.
        payload_filter : str
            only include payloads which type contains this string
        view : int
            Sets the view of the tree. Default is 1. Set 2 for stale and 3 for both.
        format : bool
            Defines whether the payload is indented or not. Default is False.

        Returns
        -------
        res : Requested scan (without the HTML status code).
        """
        res = self._standard_get(
            "/tag/tree",
            {"stage": stage, "depth": depth, "payload_data": payload_data, "payload_filter": payload_filter, "name": name, "view": view, "format": format},
            "Could not get tag tree.",
        )
        res.pop("status")
        return res

    def update_connections(self, myls: list):
        """
        Updates the view of parent-child-connections.

        Parameters
        ----------
        list : list
            List of dictionaries containing:
                "child": str
                    uuid of child
                "parent": str
                    uuid of parent
                "view": int
                    view of the connection to be set. 1 = default, 2 = stale, 3 = both

        Returns
        -------
        None.
        """
        res = self._standard_post(
            "/connections/update",
            {"connections": myls},
            {},
            "Could not update connection.",
        )

        return None

    # Database endpoints

    def read_all(
        self,
        table: str,
        filter: str = "",
        name_filter: str = "",
        payload_filter: str = "",
        child_filter: str = "",
        offset: int = 0,
        limit: int = 0,
        order_by: str = "",
        asc: bool = True,
        runkey_info: bool = False,
        payload_data: bool = False,
        depth: int = 0,
        stage: bool = True,
    ):
        """
        Gets a list of datasets from one table.

        Parameters
        ----------
        child_filter : str
            Type of child to filter by, datasets without a suitable child will not be listed (only works for direct associations).
        filter : str
            Type to filter by.
        name_filter: str
            Name to filter by.
        order_by : str
            Attribute by which to order the list
        payload_filter : str
            Type of payload to filter by (only has an effect when reading the object or tag table). Datasets without a suitable payload will not be listed (only works for direct associations).
        table : str
            Table to be listed.
        stage : bool
            Defines whether the request should be executed on the backend database (False) or staging area (True). Default is True.
        asc : bool
            Defines whether to order the list ascending or descending.
        depth : int
            Depth up to which the tree will be traversed (-1 for full tree). Default is 0.
        limit : int
            Limit after which to end the list. Default is 0.
        offset : int
            Offset by which to start the lsit. Default is 0.
        payload_data : bool
            Whether or not to add the payload data. Default is False.
        runkey_info : bool
            Whether or not to add the runkey data to each dataset. Default is False.

        Returns
        -------
        res["list"] : List of requested datasets.
        """
        res = self._standard_get(
            "/read_all",
            {
                "stage": stage,
                "asc": asc,
                "child_filter": child_filter,
                "depth": depth,
                "filter": filter,
                "name_filter": name_filter,
                "limit": limit,
                "offset": offset,
                "order_by": order_by,
                "payload_data": payload_data,
                "payload_filter": payload_filter,
                "runkey_info": runkey_info,
                "table": table,
            },
            "Could not read dataset.",
        )
        return res["list"]

    # Utilities

    # def create_runkey(self, import_path):
    #     runkey_dict = create_import_dict(import_path, "root")
    #     # Send this runkey to the staging area
    #     dic = {"data": runkey_dict}
    #     response = self._standard_post("/stage/create", dic, "Could not send runkey to staging area.")
    #     # Get id of root node from response
    #     root_id = response["id"]
    #     print(f"Imported runkey with root_id: {root_id}")
    #     return root_id

    def export_runkey(self, export_path: str, runkey_name: str, stage: bool = True):
        """
        Exports a runkey to the disk.

        Parameters
        ----------
        export_path : str
            Save path of the runkey.
        runkey_name : str
            Name of the runkey to be exported.
        stage : bool
            Defines whether the request should be executed on the backend database (False) or staging area (True). Default is True.

        Returns
        -------
        None.
        """
        counter = 0
        new_export_path = os.path.join(export_path, runkey_name, str(counter))
        while os.path.isdir(new_export_path):
            new_export_path = os.path.join(
                export_path,
                runkey_name,
                str(counter),
            )
            counter = counter + 1

        tag = self.tag_tree(runkey_name, payload_data=True, stage=stage)

        export_from_dict(new_export_path, tag)
        print(f"Exported runkey to: {new_export_path}")

        return new_export_path

    def import_runkey(
        self,
        import_path: str,
        runkey_name: str,
        author: str = "",
        comment: str = "",
        commit: bool = False,
    ):
        """
        Exports a runkey from the disk.

        Parameters
        ----------
        commit : bool
            Whether or not to commit the runkey to the database.
        import_path : str
            File path of the runkey.
        runkey_name : str
            Name of the runkey to be imported.
        author, comment: str
            author, comment of the tag, respectively.

        Returns
        -------
        None.
        """
        runkey_dict = create_import_dict(import_path, "root")
        # Send this runkey to the staging area
        root_id = self.stage_create(runkey_dict)
        # Get id of root node from response
        print(f"Imported runkey with root_id: {root_id}")

        if commit:
            resp = self.stage_commit(root_id, runkey_name, "runkey", author=author, comment=comment)
            print(f"Committed runkey with tag: {runkey_name}")
        else:
            resp = self.tag_create(runkey_name, "runkey", objects=[root_id], author=author, comment=comment)
            print(f"Tagged runkey with tag: {runkey_name}")

    def import_scan_configs(
        self,
        module_type: str = "rd53a",
        runkey_name: str = "scan_configs",
        filter: str = "",
    ):
        """
        Creates (and commits) a runkey with scan configs from yarr repo

        Parameters
        ----------
        module_type : str
            Type of the module for which the scan configs are imported (see https://gitlab.cern.ch/YARR/YARR/-/tree/master/configs/scans?ref_type=heads for available types).
            Default is "rd53a".
        runkey_name : str
            Name of the tag for the runkey. Default is "scan_configs".
        filter : str
            Filter for the scan_config type. Default is empty string.

        Returns
        -------
        None.
        """

        configs = read_scan_configs(module_type, filter=filter)

        scan_configs = []
        for config in configs:
            scan_configs.append(
                {
                    "type": config["type"],
                    "payloads": [{"data": config["data"], "type": "config"}],
                }
            )

        tree = {"type": "root", "children": scan_configs}

        id = self.stage_create(tree, type="scan_configs", author="pyconfigdb")
        self.stage_commit(id, runkey_name, "scan_configs", author="pyconfigdb")
