import os, re, urllib
from requests import get


def read_scan_configs(module_type: str, filter: str = ""):
    repo_url = "https://gitlab.cern.ch/api/v4/projects/9166/repository"
    params = {
        "path": f"configs/scans/{module_type}",
        "ref": "master",
        "per_page": 100,
    }

    url = f"{repo_url}/tree"
    response = get(url, params=params)
    if response.status_code == 200:
        items = response.json()
        configs = []
        for item in items:
            if filter:
                if filter not in item["name"]:
                    continue
            data = fetch_file_content(
                f"{repo_url}/files/{urllib.parse.quote(item['path'], safe='')}/raw?ref=master"
            )
            configs.append({"data": data, "type": item["name"].replace(".json", "")})
        return configs
    else:
        print(f"Failed to list directories: {response.status_code}")
        return None


def fetch_file_content(file_url):
    response = get(file_url)
    if response.status_code == 200:
        return response.text
    else:
        print(f"Failed to fetch file: {response.status_code}")
        return None


def export_from_dict(dir, tag):
    if not os.path.exists(dir):
        os.makedirs(dir, exist_ok=True)

    tag = tag["objects"][0]

    __export_payloads(dir, tag["payloads"])
    for object in tag["children"]:
        __loop_export_tree(dir, object)

    return dir


def __loop_export_tree(dir, data):
    dir = os.path.join(dir, f"{data['type']}~{data['id']}")
    os.makedirs(dir, exist_ok=True)
    __export_payloads(dir, data["payloads"])
    for child in data["children"]:
        __loop_export_tree(dir, child)


def __export_payloads(dir, payloads):
    for payload in payloads:
        # if "name" in payload and payload["name"] is not None:
        #     name = payload["name"]
        # else:

        name = f"{payload['type']}~{payload['id']}"
        if "meta" in payload and payload["meta"]:
            name = f"meta_{name}"
        with open(os.path.join(dir, name), "w") as file:
            file.write(payload["data"])


def create_import_dict(dir, type):
    dic = {"children": [], "payloads": [], "type": type}

    for obj in os.listdir(dir):
        if os.path.isdir(os.path.join(dir, obj)):
            child_type = re.split(r"(~?\d+)|(~\w+)", obj)[0]
            child = create_import_dict(os.path.join(dir, obj), child_type)
            dic["children"].append(child)
        else:
            dic["payloads"].append(__import_dict_payload(dir, obj))
    return dic


def __import_dict_payload(dir, obj):
    with open(os.path.join(dir, obj), "r") as file:
        data = file.read()
    payl = {"type": re.split(r"(~?\d+)|(~\w+)", obj)[0], "data": data, "name": obj}
    if "meta" in obj:
        payl["meta"] = True
    return payl


if __name__ == "__main__":
    items = read_scan_configs("rd53a", "std_")
    print("Done")
