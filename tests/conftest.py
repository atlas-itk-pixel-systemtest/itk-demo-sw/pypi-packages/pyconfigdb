import pytest
import os
from pyconfigdb.configdb import ConfigDB
from configdb_server.database_tools import Database, Backends


@pytest.fixture
def db() -> ConfigDB:

    dir = f"{os.path.dirname(os.path.abspath(__file__))}/docker"
    db = Database(Backends.SQLALCHEMY_SQLITE, f"//{dir}/backend.db")
    db.backend.upgrade_database()
    db.backend.clear_database()
    db = Database(Backends.SQLALCHEMY_SQLITE, f"//{dir}/stage.db")
    db.backend.upgrade_database()
    db.backend.clear_database()

    key = "demi/api_test/pyconfigdb/test_api/url"
    #key = "demi/default/itk-demo-configdb/api/url"
    configdb = ConfigDB(key)
    return configdb


# @pytest.fixture
# def db() -> ConfigDB:
#     key = "demi/api_test/pyconfigdb/test_api/url"
#     configdb = ConfigDB(key)
#     return configdb