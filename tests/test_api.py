from pyconfigdb.configdb import ConfigDB
import json
import pytest
import os, shutil
from dotenv import load_dotenv
from pyconfigdb.exceptions import ConfigDBResponseError

# High-level tests


def test_payload_format(db: ConfigDB):
    id = db.payload_create("pl", json.dumps({"test": 1}))
    node = db.payload_read(id, format=True)
    assert node["type"] == "pl"
    assert node["data"] == '{\n    "test": 1\n}'


def test_add_node(db: ConfigDB):
    id = db.add_node("test")
    node = db.object_read(id)
    assert node["type"] == "test"


def test_add_nodes(db: ConfigDB):
    pars = [
        {"children": [], "parents": [], "payloads": [], "type": "type0"},
        {"children": [], "parents": [], "payloads": [], "type": "type1"},
    ]
    ids = db.add_nodes(pars)
    for i, id in enumerate(ids):
        node = db.object_read(id)
        assert node["type"] == f"type{i}"


def test_add_to_node(db: ConfigDB):
    main = db.add_node("main")
    mypayloads = [{"type": "pl0", "data": "123"}, {"type": "pl1", "data": "123"}]
    payloadIDs = db.add_to_node(main, payloads=mypayloads)
    for i, id in enumerate(payloadIDs):
        payload = db.payload_read(id)
        assert payload["type"] == f"pl{i}"


def test_remove_from_node(db: ConfigDB):
    main = db.add_node("main")
    payload = db.add_to_node(main, payloads=[{"type": "pl", "data": "123"}])
    child = db.add_node("child", parents=[{"id": main, "view": 1}])
    object = db.object_read(main)
    assert len(object["payloads"]) == 1
    assert len(object["children"]) == 1

    db.remove_from_node(id=main, children=[child], payloads=payload)
    object = db.object_read(main)
    assert len(object["payloads"]) == 0
    assert len(object["children"]) == 0


def test_search(db: ConfigDB):
    main = db.add_node("main", stage=False)
    mypayloads = [
        {"type": "pl0", "meta": True, "data": json.dumps({"type0": "pl0"})},
        {"type": "pl1", "meta": True, "data": json.dumps({"type1": "pl1"})},
    ]
    payloadIDs = db.add_to_node(main, payloads=mypayloads, stage=False)
    tree = db.object_tree(main, stage=False)
    for i, id in enumerate(payloadIDs):
        res = db.search(main, search_dict={f"type{i}": f"pl{i}"})
        assert len(res) == 1


def test_search_subtree(db: ConfigDB):
    main = db.add_node(
        "main",
        payloads=[{"type": "main", "meta": True, "data": json.dumps({"type": "main"})}],
        stage=False,
    )
    child1 = db.add_node(
        "child",
        parents=[{"id": main, "view": 1}],
        stage=False,
    )
    child2 = db.add_node(
        "child",
        parents=[{"id": main, "view": 1}],
        stage=False,
    )
    grandch = db.add_node(
        "grandch",
        parents=[{"id": child1, "view": 1}],
        stage=False,
    )
    res = db.search_subtree(main, search_dict={"type": "main"})
    res1 = res[0]["children"]
    assert res1[0]["type"] == "child"
    assert res1[1]["type"] == "child"
    if len(res1[0]["children"]) != 0:
        assert res1[0]["children"][0]["type"] == "grandch"
    elif len(res1[1]["children"]) != 0:
        assert res1[1]["children"][0]["type"] == "grandch"


# Stage tests


def test_stage_new_delete(db: ConfigDB):
    id = db.stage_new(name="myrunkey")
    assert db.tag_read("myrunkey")["name"] == "myrunkey"
    db.stage_delete("myrunkey")
    scan = db.read_all("tag")
    assert len(scan) == 0


def test_stage_create(db: ConfigDB):
    data = {
        "type": "runkey",
        "children": [{"type": "child", "children": [{"type": "grandch"}]}],
    }

    tree = db.stage_create(data, name="runkey")
    res = db.tag_tree("runkey")
    assert res["objects"][0]["children"][0]["type"] == "child"
    assert res["objects"][0]["children"][0]["children"][0]["type"] == "grandch"


def test_stage_commit_clone(db: ConfigDB):
    data = {
        "type": "runkey",
        "children": [{"type": "child", "children": [{"type": "grandch"}]}],
    }

    tree = db.stage_create(data, name="runkey")

    base_id = db.stage_commit("runkey", "base_rk")
    scan_base = db.tag_tree("base_rk", stage=False)
    assert scan_base["objects"][0]["children"][0]["type"] == "child"
    assert scan_base["objects"][0]["children"][0]["children"][0]["type"] == "grandch"

    stage_id = db.stage_clone("base_rk", "stage_rk")
    scan_stage = db.tag_tree("stage_rk")
    assert scan_stage["objects"][0]["children"][0]["type"] == "child"
    assert scan_stage["objects"][0]["children"][0]["children"][0]["type"] == "grandch"


# Low-level tests


def test_insert(db: ConfigDB):
    db.insert(
        insert_list=[
            {
                "type": "main",
                "meta": True,
                "data": json.dumps({"type": "main", "data": "123"}),
            }
        ],
        table="payload",
    )

    assert len(db.read_all("payload")) == 1


def test_object_create_read(db: ConfigDB):
    id = db.object_create("test")
    node = db.object_read(id)
    assert node["type"] == "test"


def test_object_tree(db: ConfigDB):
    main = db.object_create("main")
    child = db.object_create("child", parents=[{"id": main, "view": 1}])
    grandch = db.object_create("grandch", parents=[{"id": child, "view": 1}])

    full_tree = db.object_tree(main)

    assert full_tree["children"][0]["type"] == "child"
    assert full_tree["children"][0]["children"][0]["type"] == "grandch"

    shallow = db.object_tree(main, depth=1)

    assert shallow["children"][0]["type"] == "child"
    with pytest.raises(IndexError):
        shallow["children"][0]["children"][0]["type"] == "grandch"

    # assert res1[0]["type"] == "child"
    # assert res1[1]["type"] == "child"
    # assert (
    #     res1[1]["children"][0]["type"] == "grandch"
    #     or res1[0]["children"][0]["type"] == "grandch"
    # )


def test_payload_create_read(db: ConfigDB):
    id = db.payload_create("pl")
    node = db.payload_read(id)
    assert node["type"] == "pl"


def test_tag_create_read(db: ConfigDB):
    id = db.tag_create("mytag", "tag")

    db.tag_create("member", "runkey")
    db.tag_create("group", "runkey")
    db.tag_create("root_tag", "runkey", members=["member"], groups=["group"])
    tag = db.tag_read("root_tag")

    assert tag["members"] == ["member"]
    assert tag["groups"] == ["group"]


def test_format(db: ConfigDB):
    id = db.tag_create("mytag2", "tag", author="Jonas")
    node = db.tag_format("mytag2", shorten_data=-1)
    assert node == "author: Jonas\n"


def test_tag_tree(db: ConfigDB):
    main = db.object_create("main")
    main_tag = db.tag_create("main", "main", objects=[main], author="Patricia")
    child = db.object_create("child", parents=[{"id": main, "view": 1}])
    grandch = db.object_create("grandch", parents=[{"id": child, "view": 1}])

    full_tree = db.tag_tree("main")

    assert full_tree["objects"][0]["children"][0]["type"] == "child"
    assert full_tree["objects"][0]["children"][0]["children"][0]["type"] == "grandch"
    assert full_tree["author"] == "Patricia"

    shallow = db.tag_tree("main", depth=1)

    assert shallow["objects"][0]["children"][0]["type"] == "child"
    assert shallow["author"] == "Patricia"
    with pytest.raises(IndexError):
        shallow["objects"][0]["children"][0]["children"][0]["type"] == "grandch"


def test_update_connections(db: ConfigDB):
    main = db.object_create("main")
    child1 = db.add_node("child", parents=[{"id": main, "view": 1}])
    child2 = db.add_node("child", parents=[{"id": main, "view": 1}])

    scan = db.object_tree(main, view=1)

    assert len(scan["children"]) == 2

    db.update_connections([{"child": child2, "parent": main, "view": 2}])

    scan = db.object_tree(main, view=1)

    assert len(scan["children"]) == 1


# Database tests


def test_read_all(db: ConfigDB):
    list = db.read_all("payload")
    assert len(list) == 0


def test_read_all2(db: ConfigDB):
    db.payload_create("test")
    list = db.read_all("payload")
    assert len(list) == 1


# Utility tests


def test_export_import_runkey(db: ConfigDB):
    my_path = os.path.dirname(os.path.abspath(__file__)) + "/test_files"
    main = db.object_create("main")
    child = db.object_create("child", parents=[{"id": main, "view": 1}])

    mypayloads = [{"type": "pl0", "data": "data1"}, {"type": "pl1", "data": "data2"}]
    childpl = [{"type": "cpl0", "data": "data3"}]
    payloadIDs = db.add_to_node(main, payloads=mypayloads)
    childplIDs = db.add_to_node(child, payloads=childpl)
    main_tag = db.tag_create("main", "main", objects=[main], author="Patricia")

    new_path = db.export_runkey(my_path, "main", True)

    assert os.path.isfile(f"{new_path}/pl0~{payloadIDs[0]}")
    assert os.path.isfile(f"{new_path}/pl1~{payloadIDs[1]}")
    assert os.path.isfile(f"{new_path}/child~{child}/cpl0~{childplIDs[0]}")

    db.import_runkey(new_path, "imported", commit=False)

    scan = db.tag_tree("imported", payload_data=True)
    assert scan["objects"][0]["payloads"][0]["data"] == "data1" or "data2"
    assert scan["objects"][0]["payloads"][1]["data"] == "data2" or "data1"
    assert scan["objects"][0]["children"][0]["payloads"][0]["data"] == "data3"
    assert scan["objects"][0]["payloads"][0]["type"] == "pl1" or "pl2"
    assert scan["objects"][0]["children"][0]["type"] == "child"

    shutil.rmtree(my_path)


def test_import_scan_configs(db: ConfigDB):
    db.import_scan_configs("rd53a", "scan_configs", "std_")
    scan_configs = db.tag_tree("scan_configs", stage=False)["objects"][0]["children"]
    assert len(scan_configs) > 10


def test_bundle_payloads(db: ConfigDB):
    payl1 = db.payload_create(
        "config", data=json.dumps({"a": 1, "b": [2, 3], "c": {"d": 4}, "e": 1})
    )
    payl2 = db.payload_create(
        "config", data=json.dumps({"a": 2, "b": [4, 3], "c": {"d": 4}, "e": 2})
    )
    payl3 = db.payload_create(
        "config", data=json.dumps({"a": 3, "b": [4, 5], "c": {"d": 3}, "e": 1})
    )

    obj = db.object_create("frontend", payloads=[payl1, payl2, payl3])

    changes = {"b": {0: 2}, "e": 5}
    connections = [
        {"payload_id": payl1, "object_id": obj},
        {"payload_id": payl2, "object_id": obj},
        {"payload_id": payl3, "object_id": obj},
    ]
    ids = db.payloads_change(changes, connections)
    bundle = db.payloads_bundle(ids)

    assert bundle == {
        "a": [1, 2, 3],
        "b": [[2, 2, 2], [3, 3, 5]],
        "c": {"d": [4, 4, 3]},
        "e": [5, 5, 5],
    }


def test_update_payload(db: ConfigDB):
    payload_id = db.payload_create("test_type", "test_data", "test_name", stage=True)
    db.payload_update(payload_id, "test_type2", "test_data2", "test_name2")
    payload = db.payload_read(payload_id)

    assert payload["type"] == "test_type2"
    assert payload["data"] == "test_data2"
    assert payload["name"] == "test_name2"
    assert payload["id"] == payload_id


@pytest.mark.test
def test_search_in_tag(db: ConfigDB):
    payl1 = db.payload_create(type="as", data="as_data", name="as01", stage=False)
    payl2 = db.payload_create(type="ds", data="ds_data", name="ds01", stage=False)
    payl3 = db.payload_create(type="ts", data="ts_data", name="ts01", stage=False)

    payl4 = db.payload_create(type="as", data="as_data", name="as02", stage=False)
    payl5 = db.payload_create(type="ds", data="ds_data", name="ds02", stage=False)
    payl6 = db.payload_create(type="ts", data="ts_data", name="ts02", stage=False)

    payl7 = db.payload_create(type="as", data="as_data", name="as03", stage=False)
    payl8 = db.payload_create(type="ds", data="ds_data", name="ds03", stage=False)
    payl9 = db.payload_create(type="ts", data="ts_data", name="ts03", stage=False)

    meta1 = db.payload_create(
        type="meta", data=json.dumps({"serial": "eins"}), meta=True, stage=False
    )
    meta2 = db.payload_create(
        type="meta", data=json.dumps({"serial": "zwei"}), meta=True, stage=False
    )

    obj1 = db.object_create(
        type="frontend", payloads=[payl1, payl2, payl3, meta1], stage=False
    )
    obj2 = db.object_create(
        type="frontend", payloads=[payl4, payl5, payl6, meta2], stage=False
    )

    tag = db.tag_create(
        name="tag1",
        type="scans",
        payloads=[payl1, payl2, payl3, payl4, payl5, payl6, payl7, payl8, payl9],
        stage=False,
    )

    payloads = db.search_in_tag(
        name="tag1", payload_types=["as", "ds"], object_ids=[obj1]
    )

    assert len(payloads) == 2
    assert payloads[0]["object_id"] == obj1
    assert "data" not in payloads[0]


def test_pdb_import(db: ConfigDB):
    load_dotenv()
    itkdb_access_code1 = os.getenv("itkdb_access_code1")
    itkdb_access_code2 = os.getenv("itkdb_access_code2")

    if itkdb_access_code1 is None or itkdb_access_code2 is None:
        raise ConfigDBResponseError("Environment variables not set")

    db.pdb_import(
        itkdb_access_code1,
        itkdb_access_code2,
        name="my_module_runkey",
        serials=["20UPGM22110466", "20UPGM22110329"],
    )

    scan = db.tag_tree("my_module_runkey", payload_data=True)["objects"][0]

    assert len(scan["children"]) == 2
    assert len(scan["children"][0]["children"]) == 4
    assert len(scan["children"][0]["children"][0]["payloads"][0]["data"]) >= 10

    # assert db.tag_read("my_module_runkey")["name"] == "my_module_runkey"
