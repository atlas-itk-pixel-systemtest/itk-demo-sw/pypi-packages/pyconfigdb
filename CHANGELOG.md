# Changelog

All notable changes[^1] to the `pyconfigdb` package

[^1]: The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

(always keep this section to take note of untagged changes at HEAD)

## [2.2.3] - 2024-09-09
- Added id param to add_node

## [2.2.2] - 2024-09-05
- Added additional options to search_in_tag method

## [2.2.1] - 2024-09-04
- Added keep_ids option to commit and clone

## [2.2.0] - 2024-09-03
 - Added search_in_tag endpoint to search for payload from specific object inside a tag

## [2.1.0] - 2024-08-28
 - Added groups and members field to create_tag method

## [2.0.7] - 2024-08-26
 - Bumped pyregistry version

## [2.0.6] - 2024-08-23
 - Readded option to edit metadata datasets in backend

## [2.0.5] - 2024-08-22
 - Added payload_filter to tree-read methods
 - Reverted change in import

## [2.0.4] - 2024-08-21
 - Added pyregistry dependency

## [2.0.3] - 2024-08-21
 - Updated dependencies

## [2.0.2] - 2024-08-20
 - Added option to set sr_url via --sr_url parameter in CLI
 - Bugfixes

## [2.0.1] - 2024-08-14
 - Bugfixes

## [2.0.0] - 2024-08-06
 - Compatibility with configdb-tag 2.0.0 (fast-api)
 - BREAKING CHANGE: give srUrl when instanciating the configdb class!

## [1.1.9] - 2024-08-09
- Added health function

## [1.1.8] - 2024-08-08
 - Added option to edit metadata datasets in backend

## [1.1.6] - 2024-07-15
 - Further improved error handling for CLI

## [1.1.5] - 2024-07-11
 - Improved error output for CLI

## [1.1.4] - 2024-07-04
 - Added format option to payload read functions, to enable formatting of json payloads

## [1.1.3] - 2024-07-01
 - Added meta option to read and update payloads

## [1.1.2] - 2024-06-28
 - Added endpoint to update a payload in the staging area

## [1.1.1] - 2024-06-25
- Bugfix in import script

## [1.1.0] - 2024-06-23
- Changed name of CLI command from pyconfigdb to configdb
- Added function to import scan configs from yarr git repo
- Added new configdb endpoints payloads/bundle and payloads/change
- Compatible with configdb tag 1.10.1

## [1.0.7] - 2024-06-10
- Added children to add_to_node
- Made new tag name optional for commit

## [1.0.6] - 2024-06-10
- Added init file, to enable import from Configdb class directly from pyconfigdb package

## [1.0.5] - 2024-05-28
- Added update_connection
- Upgraded ConfigDB to 1.9.1

## [1.0.4] - 2024-05-28
- Small update to export: metadata payloads are now named meta_{name}

## [1.0.3] - 2024-05-15
- Properly packaged CLI
- Added view options

## [1.0.2] - 2024-05-09
- Updated to configdb tag 1.8.0
- Added author and comment to import
- Added readme

## [1.0.1] - 2024-05-09
- Added backend option to export function
- Bugfix in export to ensure recursive dir creation
- Made commit option in import-rk a flag

## [1.0.0] - 2024-05-08
- Initial features:
    - python wrapper around configdb API
    - Unittests for API
    - CLI for I/O, clone and commit
    - Docker image to access CLI